/**
 *
 * @author i.cornish
 */

package opstasktrigger;

import edi.jms.EDIRequesterTool;
import edi.jms.MsgProducer;
import javax.jms.JMSException;


public class Main {

    private String scheduleName = "OpsTaskTrigger";
    private String subjectname = "";    
    private boolean isTaskFile = false;
    private String taskFile = null;
    private boolean isStatusRequest = false;
    private String topicname = null;
    private boolean useDealLineQueue = false;
    private boolean useAuxilliaryQueue = false;
    private boolean useAuxilliaryQueue2 = false;
    private boolean useAuxilliaryQueue3 = false;
    private boolean useAuxilliaryQueue4 = false;
    private boolean useAuxilliaryQueue5 = false;
    private boolean useFTPQueue = false;
    private boolean useTestQueue = false;

    private final String msg_help = "Usage: OpsTaskTrigger [options] [arguments]\n" +
            "\n" +
            "Options:\n" +
            "  -taskfile <absolutepath and filename> <ExecutionQueueName> <Description>\n" +
            "  -status <topic name>\n";

    public Main(String[] args) {
        if (parseArguments(args)) {
            //taskFile = args[0];
            sendMessage(args);
            //System.exit(0);
        } else {
            //System.exit( 1 ); // Invalid Parameters
        }
    }

    private boolean parseArguments(String[] args) {
        if (args.length == 0) {
            //JOptionPane.showMessageDialog(null, "Automated Operations Task Trigger", msg_help, JOptionPane.INFORMATION_MESSAGE);
        }

        try {
            for (int i = 0; i < args.length; i++) {
                if (args[i].compareToIgnoreCase("-taskfile") == 0) {
                    this.isTaskFile = true;
                    this.taskFile = args[i + 1];
                    try{
                        this.scheduleName = args[i + 3];
                    }catch(ArrayIndexOutOfBoundsException e){
                    }
                }

                if (args[i].compareToIgnoreCase("-status") == 0) {
                    this.isStatusRequest = true;
                    this.topicname = args[i + 1];
                }

                if (args[i].compareToIgnoreCase("-deadline") == 0) {
                    useDealLineQueue = true;
                }
                if (args[i].compareToIgnoreCase("-auxilliary") == 0) {
                    useAuxilliaryQueue = true;
                }
                if (args[i].compareToIgnoreCase("-auxilliary2") == 0) {
                    useAuxilliaryQueue2 = true;
                }
                if (args[i].compareToIgnoreCase("-auxilliary3") == 0) {
                    useAuxilliaryQueue3 = true;
                }
                if (args[i].compareToIgnoreCase("-auxilliary4") == 0) {
                    useAuxilliaryQueue4 = true;
                }
                if (args[i].compareToIgnoreCase("-auxilliary5") == 0) {
                    useAuxilliaryQueue5 = true;
                }
                if (args[i].compareToIgnoreCase("-ftp") == 0) {
                    useFTPQueue = true;
                }
                if (args[i].compareToIgnoreCase("-test") == 0) {
                    useTestQueue = true;
                }

            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private void sendMessage(String[] args) {

        // Produce a Status request Message and create a JMSListener for receiving the
        // the status request message
        if (isStatusRequest) {
            try {
                // Create the requester tool
                EDIRequesterTool reqTool = new EDIRequesterTool(args,"OPS.EXECUTION");

                // Set the tools properties
                reqTool.setTopic(true); // Set the Requester tool to use a topic for sending.                
                reqTool.setReplySubject("OPS.TASKTRIGGER"); // Where the replys should be sent to.

                // Set the custom message properties
                reqTool.addBooleanProp("isStatusRequest", true);
                reqTool.addMessageBody("#" + new java.util.Date().toGMTString() + "#");

                // Run the tool
                reqTool.run();

                return;
            } catch (JMSException ex) {
            }
        }

        if (isTaskFile) {
            // Produce OPS.EXECUTION MESSAGE for scheduled task
            MsgProducer producer = new MsgProducer(args);
            if (useDealLineQueue) {
                producer.setMessageType(producer.msgtype_deallinetasks);
            } else if (useAuxilliaryQueue) {
                producer.setMessageType(producer.msgtype_auxiliarytasks);
            } else if (useAuxilliaryQueue2) {
                producer.setMessageType(producer.msgtype_auxiliarytasks2);
            } else if (useAuxilliaryQueue3) {
                producer.setMessageType(producer.msgtype_auxiliarytasks3);
            } else if (useAuxilliaryQueue4) {
                producer.setMessageType(producer.msgtype_auxiliarytasks4);
            } else if (useAuxilliaryQueue5) {
                producer.setMessageType(producer.msgtype_auxiliarytasks5);
            } else if (useFTPQueue) {
                producer.setMessageType(producer.msgtype_FTPtasks);
            } else if (useTestQueue) {
                producer.setMessageType(producer.msgtype_Testtasks);
            } else {
                producer.setMessageType(producer.msgtype_Task);
            }

            producer.setTaskFile( taskFile );
            producer.setScheduleName( scheduleName );
            producer.setBody( "body" );
            producer.sendMessage( );

            // Dispose of the producer and do a garbage collection
            producer = null;
            System.gc();
            return;
        }
    }

    public static void main(String[] args) {
        new Main(args);
    }

}
